# Logx

A Node.js module for logging purposes. Easily log to the console, having always the same formatting:

```sh
[TYPE_OF_LOG]: [DATE] [MESSAGE]
```

Examples:

![Some examples](assets/examples.png)

## Installation

To install this package, simply type the following command in your terminal:

```sh
npm i @ngox/logx
```

Of course, `npm` must be installed on your computer.

## Usage

### Javascript

```javascript
var Logger = require('xxx').Logger;
var LEVEL = require('xxx').LEVEL;

Logger.log('Foo');
Logger.logx(LEVEL.ERROR, 'Bar');
```

### TypeScript

```typescript
import { LEVEL, Logger } from 'xxx';

Logger.log('Foo');
Logger.logx(LEVEL.ERROR, 'Bar');
```

### Output expected

![Output expected](assets/output.png)

## Test

```sh
npm run test
```
