class Logx {
  /**
   * Basic logging method.
   *
   * Simply pass the message you need to log to the method parameters.
   * Use it for informative logs. For more control over log level, use `logx` method.
   *
   * @param msg Message to log
   */
  static log(msg: string) {
    this.logx(LEVEL.INFO, msg);
  }

  /**
   * Logging method with log levels.
   *
   * Pass the log level and the message to log.
   * Log level will change the color of the log.
   *
   * @param level Log level
   * @param msg Message to log
   */
  static logx(level: LEVEL, msg: string) {
    console.log(this.display(level) + msg + this.resetColor());
  }

  private static display(level: LEVEL) {
    return level + new Date().toLocaleString('en-GB', { hour12: false }) + '\t';
  }

  private static resetColor() {
    return '\x1b[0m';
  }
}

/**
 * Enumeration for the logging levels.
 *
 * Each level has it's own color.
 *
 * `DEBUG: Blue`
 * `INFO: Green`
 * `WARNING: Orange`
 * `ERROR: Red`
 */
enum LEVEL {
  DEBUG = '\x1b[34m DEBUG: \t',
  INFO = '\x1b[32m INFO: \t\t',
  WARNING = '\x1b[33m WARN: \t\t',
  ERROR = '\x1b[31m ERROR: \t',
}

export { LEVEL, Logx as Logger };
