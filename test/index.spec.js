'use strict';

// TODO: Make es6 tests.
// import { expect } from 'chai';
// import { stdout } from 'test-console';
var expect = require('chai').expect;
var stdout = require('test-console').stdout;
var Logger = require('../dist/index.js').Logger;
var LEVEL = require('../dist/index.js').LEVEL;

const DATE = new Date().toLocaleString('en-GB', { hour12: false }) + '\t';
const END = '\x1b[0m\n';

describe('Logger test', () => {
  it('log:\t Testing default log', () => {
    const output = stdout.inspectSync(() => {
      Logger.log('foo');
    });

    expect(output).to.deep.equal([LEVEL.INFO + DATE + 'foo' + END]);
  });
  it('logx:\t Testing DEBUG log', () => {
    const output = stdout.inspectSync(() => {
      Logger.logx(LEVEL.DEBUG, 'foo');
    });

    expect(output).to.deep.equal([LEVEL.DEBUG + DATE + 'foo' + END]);
  });
  it('logx:\t Testing INFO log', () => {
    const output = stdout.inspectSync(() => {
      Logger.logx(LEVEL.INFO, 'foo');
    });

    expect(output).to.deep.equal([LEVEL.INFO + DATE + 'foo' + END]);
  });
  it('logx:\t Testing WARNING log', () => {
    const output = stdout.inspectSync(() => {
      Logger.logx(LEVEL.WARNING, 'foo');
    });

    expect(output).to.deep.equal([LEVEL.WARNING + DATE + 'foo' + END]);
  });
  it('logx:\t Testing ERROR log', () => {
    const output = stdout.inspectSync(() => {
      Logger.logx(LEVEL.ERROR, 'foo');
    });

    expect(output).to.deep.equal([LEVEL.ERROR + DATE + 'foo' + END]);
  });
});
